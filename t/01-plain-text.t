use strict;
use warnings;
use Test::More;
use MIME::Lite;

my $body = MIME::Lite->new( Type => "multipart/alternative" );

$body->attach(
    Type     => "TEXT",
    Encoding => 'quoted-printable',
    Data     => "Hi There",
);

$body->attach(
    Type     => "text/html",
    Encoding => 'quoted-printable',
    Data     => "<h1>Hi</h1>"
);

my $mail = MIME::Lite->new(
    From    => q{m.vu@cv-library.co.uk},
    To      => q{m.vu@cv-library.co.uk},
    Subject => q{Test Plain Text},
    Type    => "multipart/mixed",
);

$mail->attach($body);

$mail->attach(... );

print $mail->as_string;

$mail->send( smtp => "192.168.1.42:25" );
